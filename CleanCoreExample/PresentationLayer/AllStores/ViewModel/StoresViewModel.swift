//
//  StoresViewModel.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 13.01.17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

fileprivate let INDEX_BAR_DIGIT_TITLE = "#"

class StoresViewModel: BaseTableViewModel <StoreCellViewModel, [AllStoresStore], AllStoresEntity> {
    
    public func subscribeToData()
    {
        super.subscribeToData(params: AllStoresRequestParams.self).subscribe(
            
            onNext: { [weak self]  (data:[AllStoresStore]) in
            
                self?.parseModels(response: data)
            
            },
            onError: {[weak self] (error:Error) in
        
                if (error as NSError).code == NSURLErrorNotConnectedToInternet
                {
                    AlertManager.shared.showConfirm(title: NSLocalizedString("Oooops", comment: "Not internet connection alert title"),
                                                    message: NSLocalizedString("Please check your internet connection and try again", comment: "Not internet connection alert text"),
                                                    actions:[NSLocalizedString("Try Again", comment: "Not internet connection alert button label text"): { self?.subscribeToData() }]
                    )
                }
            }
            
        ).addDisposableTo(disposeBag!)
    }
    
    internal override func parseModels(response: [AllStoresStore])
    {
        var sectionNames: [String] = [String]()
        var sectionItems: [String:[StoreCellViewModel]] = [String:[StoreCellViewModel]]()

        let stores = response.sorted { $0.name!.localizedCaseInsensitiveCompare($1.name!) == ComparisonResult.orderedAscending }
        
        for store in stores
        {
            var firstLetter = store.name![0].uppercased()
            firstLetter = firstLetter.rangeOfCharacter(from: .decimalDigits) != nil ? "#" : firstLetter
            
            if !sectionNames.contains(firstLetter)
            {
                sectionNames.append(firstLetter)
                sectionItems[firstLetter] = [StoreCellViewModel]()
            }
            
            sectionItems[firstLetter]?.append(getCellViewModel(store:store))
        }

        self.sections.value = self.makeSectionsDataForTableView(sectionNames: sectionNames, sectionItems: sectionItems)
    }
    
    internal override func getCellViewModel(store:AllStoresStore) -> StoreCellViewModel
    {
        let storeCellViewModel:StoreCellViewModel = StoreCellViewModel(network: Network())        
        storeCellViewModel.setId(id: Int(store.id))
        storeCellViewModel.setName(name: store.name)
        storeCellViewModel.setLogoUrl(logoUrl: store.logo_url)

        return storeCellViewModel
    }   
}

