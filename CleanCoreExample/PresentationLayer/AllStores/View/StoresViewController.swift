//
//  StoresViewController.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 13.01.17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

fileprivate let CELL_NIB_NAME = "StoreTableCell"
fileprivate let CELL_REUSE_IDENTIFIER = "storeTableViewCell"

fileprivate let HEADER_NIB_NAME = "StoreTableViewHeader"
fileprivate let HEADER_REUSE_IDENTIFIER = "storeTableViewHeader"

fileprivate let CELL_HEIGHT:CGFloat = 55.0
fileprivate let SECTION_HEADER_HEIGHT:CGFloat = 29.0

fileprivate let INDEX_BAR_TITLES_COLOR = UIColor(netHex: 0x6ea627)

import UIKit

class StoresViewController: UIViewController
{
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    public var viewModel: StoresViewModel?
    private var disposeBag = DisposeBag()
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.configureTableView()
        self.setupBindings()
        
        self.activityIndicator?.color = UIColor(netHex: 0x8b8a8f)
        self.activityIndicator.hidesWhenStopped = true
    }
    
    private func configureTableView()
    {
        self.tableView.sectionIndexColor = INDEX_BAR_TITLES_COLOR
        self.tableView.register(UINib(nibName: CELL_NIB_NAME, bundle: nil), forCellReuseIdentifier: CELL_REUSE_IDENTIFIER)
        self.tableView.register(UINib(nibName: HEADER_NIB_NAME, bundle: nil), forHeaderFooterViewReuseIdentifier: HEADER_REUSE_IDENTIFIER)
        self.tableView.rowHeight = CELL_HEIGHT
        
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, StoreCellViewModel>>()
        dataSource.configureCell = { (dataSource, tv, indexPath, element) in
            
            let cell:StoreTableViewCell = tv.dequeueReusableCell(withIdentifier: CELL_REUSE_IDENTIFIER)! as! StoreTableViewCell
            cell.setViewModel(viewModel: element)
            
            return cell
        }
        
        dataSource.sectionIndexTitles = { (dataSource) in
        
            return self.viewModel?.indexBarTitles()
        }

        self.viewModel?.sections.asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .addDisposableTo(disposeBag)
        
        self.tableView.rx.setDelegate(self)
            .addDisposableTo(disposeBag)
    }
    
    private func setupBindings()
    {
        self.viewModel?.isLoading.asDriverOnErrorJustComplete().drive(onNext: {[weak self] (isLoading:Bool) in
            
            if isLoading
            {
                self?.activityIndicator?.startAnimating()
            }
            else
            {
                self?.activityIndicator?.stopAnimating()
            }
            
            self?.activityIndicator.isHidden = !isLoading
            
        }).addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        viewModel?.subscribeToData()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        self.viewModel?.unsubscribeFromData()
    }
}

extension StoresViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return SECTION_HEADER_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: HEADER_REUSE_IDENTIFIER) as! StoreTableViewHeader
        headerView.setText(text: (self.viewModel?.sections.value.count)! > 0 ? (self.viewModel?.sections.value[section].model)! : "")
        
        return headerView
    }
}
