//
//  StoreTableViewHeader.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 04.02.17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

fileprivate let SECTION_TITLES_COLOR = UIColor(netHex: 0x6ea627)
fileprivate let HEADER_COLOR = UIColor(netHex: 0xefeff4)

class StoreTableViewHeader: UITableViewHeaderFooterView
{
    @IBOutlet var label: UILabel!
    @IBOutlet var headerBackgroundView: UIView!
    
    override func awakeFromNib()
    {
        self.label.textColor = SECTION_TITLES_COLOR
        self.headerBackgroundView.backgroundColor = HEADER_COLOR
    }
    
    func setText(text:String)
    {
        self.label.text = text
    }
}
