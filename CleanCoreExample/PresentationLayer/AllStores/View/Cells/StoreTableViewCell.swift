//
//  StoreTableViewCell.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 17.01.17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

fileprivate let LOGO_BORDER_COLOR = UIColor(netHex: 0xc8c7cc)
fileprivate let TITLE_LABEL_COLOR = UIColor.black
fileprivate let NO_LOGO_IMAGE_NAME = "no_logo"

class StoreTableViewCell: UITableViewCell
{
    @IBOutlet var logo: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var logoContainer: UIView!
    
    private var logoSubscribe:Disposable?
    private var titleSubscribe:Disposable?
   
    var viewModel:StoreCellViewModel? = nil
    
    override func awakeFromNib()
    {
        super.awakeFromNib()

        self.logoContainer.layer.borderWidth = 1
        
        setupColors()
    }
    
    func setViewModel(viewModel:StoreCellViewModel)
    {
        self.viewModel = viewModel
        
        self.setFieldsBinding()
    }
    
    func setFieldsBinding()
    {
        self.titleSubscribe = self.viewModel!.name.asDriver().drive(
            
            onNext: { [weak self] results in

                self?.title.text = results
            }
        )
        
        logoSubscribe = self.viewModel?.getLogoImage()
            .asDriver(onErrorJustReturn: UIImage(named: NO_LOGO_IMAGE_NAME)!)
            .drive(
            
                onNext: { [weak self] (image:UIImage) in
                
                    self?.logo.image = image
                }
            )
    }
    
    override func prepareForReuse()
    {
        super.prepareForReuse()
        
        self.logoSubscribe?.dispose()
        self.titleSubscribe?.dispose()
        
        self.logo.image = UIImage(named: NO_LOGO_IMAGE_NAME)
    }
    
    func setupColors()
    {
        self.logoContainer.layer.borderColor = LOGO_BORDER_COLOR.cgColor
        self.title.textColor = TITLE_LABEL_COLOR
    }
}
