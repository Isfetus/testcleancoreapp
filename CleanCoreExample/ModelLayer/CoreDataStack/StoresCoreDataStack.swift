//
//  StoresCoreDataStack.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 27/07/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

class StoresCoreDataStack: CoreDataStack
{
    public init()
    {
        super.init(contextPath: "DataModel")
    }
}
