//
//  AllStoresEntity+CoreDataProperties.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 12/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import Foundation
import CoreData


extension AllStoresEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AllStoresEntity> {
        return NSFetchRequest<AllStoresEntity>(entityName: "AllStoresEntity")
    }
}
