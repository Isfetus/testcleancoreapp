//
//  AllStoresParams.swift
//  CleanCoreExample
//
//  Created by Andrey Belkin on 10/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//
import Alamofire

internal struct AllStoresRequestParams:CachableRequestParameters
{
    static var headers: [String : String]? = nil

    internal static var predicate: String? = nil

    internal static var apiURL:String = "https://amikash.ca/api/v1/stores"
    internal static var requestParameters: [String: Any]? =  nil
    internal static var method: HTTPMethod
    {
        return .get
    }
}
